/* Gulpfile */

var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var cleanCss = require('gulp-clean-css');
var browserSync = require('browser-sync');

var sourceHTML = './resource/html/*.pug';
var destHTML = './public/';
var HTMLReload = './public/*.html';

var sourceJs = './resource/js/*.js';
var destJs = './public/js/';
var JsReload = './public/js/*.js';

var sourceSass = './resource/sass/**/*.scss';
var destSass = './public/css/';
var SassReload = './public/css/*.css';

var destServe = './public';

/**/
gulp.task('js', function() {
    
});

gulp.task('pug', function() {
    return gulp.src(sourceHTML)
      .pipe(pug({
      	pretty: true
      }))
      .pipe(gulp.dest(destHTML));
});

gulp.task('sass', function() {
    return gulp.src(sourceSass)
      .pipe(sourcemaps.init({
      	loadMaps: true
      }))
      .pipe(sass())
      .pipe(autoprefixer())
      .pipe(sourcemaps.write('/'))
      .pipe(gulp.dest(destSass))
      .pipe(rename({
        suffix: '.min'
      }))
      .pipe(cleanCss())
      .pipe(gulp.dest(destSass));
});

gulp.task('serve', function() {
    browserSync.init({
    	server: {
        baseDir: destServe
      }
    })

    gulp.watch(sourceJs, ['js']);
    gulp.watch(sourceHTML, ['pug']);
    gulp.watch(sourceSass, ['sass']);
    gulp.watch(JsReload).on('change', browserSync.reload);
    gulp.watch(HTMLReload).on('change', browserSync.reload);
    gulp.watch(SassReload).on('change', browserSync.reload);
});

gulp.task('default', ['js', 'pug', 'sass', 'serve']);